require_relative 'boot'

require 'rails/all'
# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module FinalApp
  class Application < Rails::Application

    I18n.config.enforce_available_locales = true
    config.i18n.load_path += Dir[Rails.root.join('my','locales','*.{rb,yml}')]
    I18n.available_locales = [:en,:pt,:vi]
    I18n.default_locale = :en
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1
    # add I18n-js
    config.middleware.use I18n::JS::Middleware
    # caching mailer
    config.action_mailer.perform_caching = true
    #config.action_controller.asset_host= "assets.example.com"

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

  end
end
