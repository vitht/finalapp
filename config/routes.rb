Rails.application.routes.draw do
  root 'home#index'
  devise_for :users
  resources :users do
    member do
      post :follow_user, as: 'follow'
      get :notification, as: 'notify'
      get :timeline, as: 'timeline'
    end
  end

  namespace :admin do
    resources :images
    resources :users do
      member do
        post :disable_user, as: 'disable'
      end
    end
  end
  resources :images do
    collection do
      get :my_images
      get :public_images
    end
    member do
      post :like
    end
  end

  resources :albums, except: [:index] do
    collection do
      get :my_albums, as: 'my_albums'
      get :public_albums, as: 'public_albums'

    end
    member do
      post :like
      post :follow_album, as: 'follow'
    end
    resources :images, only: [:index, :create, :new]
  end

  resources :statuses do
    member do
      post :like
    end
  end



  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
