class AddIndexToColumn < ActiveRecord::Migration[5.1]
  def change
    add_index :albums, :title
    add_index :images, :title
  end
end
