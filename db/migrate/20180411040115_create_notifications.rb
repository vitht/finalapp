class CreateNotifications < ActiveRecord::Migration[5.1]
  def change
    create_table :notifications do |t|
      t.belongs_to :user
      t.text :content
      t.integer :object_id
      t.boolean :is_readed, default: true
      t.timestamps
    end
  end
end
