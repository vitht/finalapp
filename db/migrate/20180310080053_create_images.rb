class CreateImages < ActiveRecord::Migration[5.1]
  def change
    create_table :images do |t|
      t.belongs_to :album
      t.belongs_to :user
      t.string :title
      t.text :description
      t.boolean :is_private, default: false
      t.boolean :status
      t.timestamps
    end
  end
end
