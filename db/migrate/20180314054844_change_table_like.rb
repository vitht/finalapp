class ChangeTableLike < ActiveRecord::Migration[5.1]
  def change
    add_reference :likes, :likeable, polymorphic:true
  end
end
