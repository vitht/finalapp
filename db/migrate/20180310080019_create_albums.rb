class CreateAlbums < ActiveRecord::Migration[5.1]
  def change
    create_table :albums do |t|
      t.belongs_to :user
      t.string :title
      t.text :description
      t.integer :quality, default: 0
      t.boolean :is_private, default: false
      t.boolean :default
      t.timestamps
    end
  end
end

