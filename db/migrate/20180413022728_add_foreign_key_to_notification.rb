class AddForeignKeyToNotification < ActiveRecord::Migration[5.1]
  def change
    add_foreign_key :notifications, :users, column: :object_id
  end
end
