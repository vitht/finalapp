class CreateHistories < ActiveRecord::Migration[5.1]
  def change
    create_table :histories do |t|
      t.belongs_to :user
      t.references :itemable, polymorphic: true
      t.timestamps
    end
  end
end
