class CreateFollows < ActiveRecord::Migration[5.1]
  def change
    create_table :follows do |t|
      t.belongs_to :user
      t.references :followable, polymorphic: true
      t.timestamps
    end
  end
end
