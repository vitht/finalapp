class CreateStatuses < ActiveRecord::Migration[5.1]
  def change
    create_table :statuses do |t|
      t.belongs_to :user
      t.text :content
      t.boolean :is_private, default: false
      t.timestamps
    end
  end
end
