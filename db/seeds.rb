# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create([
  {first_name:"Vi",last_name:"Tran Hoang Thao",email:"tranhoangthaovi@gmail.com",password:"123456",phone:"0907456789",dob:"12/10/1996",is_admin:true},
  {first_name:"Huy",last_name:"Nguyen",email:"vitht1210@gmail.com",password:"123456",phone:"0907456789",dob:"12/10/1996",is_admin:false},
   {first_name:"Nhu",last_name:"Nguyen",email:"nhunguyen@gmail.com",password:"123456",phone:"0907456789",dob:"10/10/1996",is_admin:false},
  ])

