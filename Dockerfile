FROM docker.nustechnology.com/docker-rvm:latest
MAINTAINER hiepnh@nustechnology.com
# Install rvm, default ruby version and bundler.
ENV RUBY_VERSION 2.4.3

ENV APP_HOME /devs
RUN mkdir -p $APP_HOME
WORKDIR $APP_HOME
# Install ruby version and bundler.
COPY .ruby-version $APP_HOME/.ruby-version

RUN echo 'source /etc/profile.d/rvm.sh' >> /etc/profile && \
    /bin/bash -l -c "rvm install $RUBY_VERSION" && \
    /bin/bash -l -c "rvm use --default $RUBY_VERSION && \
    gem install bundler"

# Set up gems
COPY Gemfile* $APP_HOME/
ENV BUNDLE_PATH /bundle
ADD . $APP_HOME
RUN /bin/bash -l -c "gem install bundler"
RUN /bin/bash -l -c "bundle install"
