require 'rails_helper'

RSpec.describe User, type: :model do
  # before(:each) do
  #   let :user {User.create()}
  # end

  # it "image should belong to user" do
  #   user = User.create(first_name: "Vi")
  #   image = user.images.create(title: "Tran Hoang Thao Vi")
  #   expect(image.user_id).to eq (user.id)
  #   # expect(user.errors.keys).to include(:title)
  #   # image.title.to == "Tran Hoang Thao Vi" # user.image.to equal("Tran Hoang Thao Vi")
  #   # image.status.to == true #image.status.to be_truthy
  #   # image.status.to == false #image.status.to be falsey

  # end
  it "should validate length of title", type: 'special' do
    image = Image.create(title:"This is image title" * 20)
    expect(image.errors.keys).to include(:title)
    expect(image.errors[:title]).to include(I18n.t('errors.messages.too_long',count: 255))
  end

  it 'should validate valid of phone' do
    user= User.create(phone:12345678934)
    expect(user.phone.length).to be <= 11
  end

  it 'should validate each user have an unique email' do
    user = User.create(email:"thaovi@gmail.com")
    expect(User.where(email:user.email).count).to eq 1
    user1 = User.create(email:"thaovi@gmail.com")
    expect(User.exists?(email:user1.email)).to be_falsey
  end

  #  it "should validate begining of phone with '09'" do
  #   user = User.create(phone:0937878031)
  #   expect(user.phone.first).to start_with(0)
  # end


  # it "should create new album" do
  #   expectation = expect { post: :create, blog: FactoryGirl.build(:blog).attributes }
  #   expect(user.blogs.size).to == 1

  # end


  #expect(status).to be(true)

  # count_images = Image.where(user_id:user.id).count
  # expect(count_images).to equal(3)
  # expect(count_images).to be > 3
  # expect(count_images).to be <= 3
  # expect(count_images).to be >= 3
  # expect(count_images).to be < 3
  # expect([1,2,3]).to match_array([1,2,3])
end
