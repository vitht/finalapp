require 'rails_helper'
RSpec.describe Image, type: :model  do

  context "Associations" do
    it { should belong_to(:user) }
  end
  it "should change mount of images follow user when creating image" do
    user = User.create
    expect{ user.images.create }.to change { user.images.count }.by 1
  end

  it "should show image list of current user" do
    #get :index
    user = User.create
    expect(assigns(:images)).to include(user.images)
  end

end
