require 'rails_helper'
RSpec.describe Album, type: :model do

  context "Associations" do
    it { should have_many(:images) }
  end

  it "should delete image when delete albums" do
    # Create albums
    album = Album.create
    album.save
    # Create images for albums
    3.times do
      album.images.create
    end
    # If you have 3 images
    expect(album.images.count).to eq 3
    album.destroy
    expect(Album.exists?(id: album.id)).to be_falsey
    expect(Image.count).to eq 0
  end



  it 'test to change method' do
    k = 0
    s = 'Vy'
    expect { k += 1.05 }.to change { k }.by( a_value_within(0.1).of(1.0) )

    expect{ s =  "Vi" }.to change{ s }.from(a_string_matching(/Vy/)).to(a_string_matching(/Vi/))
  end




end
