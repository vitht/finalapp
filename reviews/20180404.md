# NUS REVIEW CODE RESULT FOR VI's FINAL APP

### Issues
---------

#### Architecture

1/ All logic code are placed in controllers. It makes the application is difficult to be maintained or reused (e.g: API for mobile app in the future). We should use additional layer for business code (e.g: "service" layer)


2/ Routes is bad. It does not follow RESful/RESOURCEful

#### Security

1/ Almost no authorization. This is BIG security issue. E.g: in products controller, **anyone** can update/delete **any product**

  - app/controllers/albums_controller.rb
  - app/controllers/images_controller.rb

#### Performance

1/ Many indexes are missing, this will cause critical performance issues when data grows

  - albums
  - images

#### Coding conventions & best practices

1/ Remove unused files, code.

  - app/assets/images/
  - app/controllers/users/
  - app/views/...


2/ Do not commit debug code. User gitk to review what we change before committing.

  - app/assets/javascripts/login.js
  - app/controllers/images_controller.rb#like


3/ Need to handle FAILURE case when create/update an item

  - app/controllers/images_controller.rb
  - app/controllers/albums_controller.rb


4/ Coding style is too ugly!


5/ Do not use `default_url` if we do not have default file.

  - app/models/user.rb


6/ I18n was not used. This is bad because it will be hard if we want to have multi-language feature in the future; using I18n also helps us manage texts better too


7/ In controller/view, we do not need to call `I18n.t` explicit, just `t` is enough.


8/ Manage users is a feature which be only accessed by admin. Should implement this feature in namespace admin and must do authorization for this feature.


9/ Almost no power of ActiveRecord was used: scope, association

  - app/controllers/images_controller.rb
  - app/controllers/albums_controller.rb


10/ Do not use inline js

  - app/views/layouts/user.html.slim


11/ UI of the app is not consistent between the pages.