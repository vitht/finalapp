# Preview all emails at http://localhost:3000/rails/mailers/user_mail_mailer
class UserMailMailerPreview < ActionMailer::Preview

  def welcome_email
    UserMailMailer.welcome_email(User.first)
  end

end
