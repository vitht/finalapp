class FollowService < BaseService

  def create_follow object
    object.follows.create(user_id: current_user.id)
  end

  def follow object
    if Follow.where(user_id: current_user.id, followable: object).exists?
      follow = Follow.where(user_id: current_user.id, followable: object)
      follow.take.delete
    else
      create_follow(object)
    end
  end

  def check_follow? object
    Follow.where(user_id: current_user.id, followable: object).exists?
  end


end
