class AlbumService < BaseService

  def get_albums params
    if params[:action] == "my_albums"
      @albums = current_user.albums.page(params[:page]).per(12).order(id: :desc)
    else
      @albums = Album.where.not(user_id:current_user).page(params[:page]).per(12)
    end
  end

  def create_album params,album_params
    @album = current_user.albums.create(album_params)
    params[:album][:image].each do |image|

      @album.images.create(user_id:current_user.id,is_private:params[:album][:is_private],image: image)
    end
    unless @album.is_private
      notify_album(current_user,"create_album")
      history_album(current_user,@album)
    end
  end #def block

  def update_album album, album_params
    album.update(album_params)
    unless @album.is_private
      notify_album(current_user,"update_album")
    end
  end

  def notify_album current_user,content
    if current_user.follower_ids.present?
      @notify = NotificationService.new
      current_user.followers.each do |follower|
        @notify.create_notification follower,content,current_user
      end # each block
    end # if block
  end

  def delete_album album
    album.destroy
    notify_album(current_user,"delete_album")


  end

  def like_album album
    @like = Like.where(user_id:current_user.id,likeable:album)
    if @like.exists?
      @like.take.delete
    else
      @like = album.likes.create(user_id:current_user.id)
    end
  end


  def history_album current_user,object
    @history = HistoryService.new
    @history.create_history(current_user,object)
  end

end
