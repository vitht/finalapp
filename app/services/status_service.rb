class StatusService < BaseService

  def create_status status_params
    @status = current_user.statuses.create(status_params)
    unless @status.is_private
      notify_status(current_user,"create_status")
      history_status(current_user,@status)
    end
  end

  def notify_status current_user,content
    if current_user.follower_ids.present?
      @notify = NotificationService.new
      current_user.followers.each do |follower|
        @notify.create_notification follower,content,current_user
      end # each block
    end # if block
  end

  def like_status status
    @like = current_user.likes.create(likeable:status)

  end

  def history_status current_user,object
    @history = HistoryService.new
    @history.create_history(current_user,object)
  end
end
