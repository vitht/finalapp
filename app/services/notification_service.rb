class NotificationService < BaseService


  def get_notification_list params
    page = (params[:page] || 1).to_i
    per_page = (params[:per_page] || DEFAULT_PER_PAGE).to_i
    return page, per_page, Notification.page(params[:page]).order(id: :desc)
  end

  def create_notification owner,content,object
    case content.to_sym
    when :follow_user
      content = I18n.t("notifications.content.follow_user")
    when :follow_album
      content = I18n.t("notifications.content.follow_album",user: object.first_name)
    when :create_album
      content = I18n.t("notifications.content.create_album",user: object.first_name)
    when :update_album
      content = I18n.t("notifications.content.update_album",user: object.first_name)
    when :delete_album
      content = I18n.t("notifications.content.delete_album",user: object.first_name)
    when :create_image
      content = I18n.t("notifications.content.create_image",user: object.first_name)
    when :update_image
      content = I18n.t("notifications.content.update_image",user: object.first_name)
    when :delete_image
      content = I18n.t("notifications.content.delete_image",user: object.first_name)
    when :create_status
      content = I18n.t("notifications.content.create_status",user: object.first_name)
    end
    owner.notifications.create(object_id: object.id, content: content)
  end

end
