class HistoryService < BaseService

  def create_history current_user,object
    History.create(user_id:current_user.id,itemable:object)
  end

  def get_histories params
    History.order(created_at: :desc).page(params[:page]).per(10)
  end
end
