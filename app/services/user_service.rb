class UserService < BaseService
  def get_users_list params
    page = (params[:page] || 1).to_i
    per_page = (params[:per_page] || DEFAULT_PER_PAGE).to_i
    return page, per_page, User.order(first_name: :asc).page(params[:page])
  end

  def disable_user user
    if user.is_active
      user.update(is_active:false)
    else
      user.update(is_active:true)
    end
  end

end
