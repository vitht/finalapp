class ImageService < BaseService

  def get_images_list params
    page = (params[:page] || 1).to_i
    per_page = (params[:per_page] || DEFAULT_PER_PAGE).to_i
    return page, per_page, Image.page(params[:page]).order(id: :desc)
  end

  def get_images params
    if params[:action] == "my_images"
      @images = current_user.images.page(params[:page]).per(12).order(id: :desc)
    else
      @images = Image.where.not(user_id:current_user).where(is_private:false).page(params[:page]).per(12).order(id: :desc)
    end
  end

  def add_image image_params
    @image = Image.create(image_params)
    unless @image.is_private
      notify_image(current_user,"create_image")
      history_image(current_user,@image)

    end
  end

  def update_image image,image_params
    image.update(image_params)
    unless image.is_private
      notify_image(current_user,"update_image")
    end
  end

  def delete_image image
    image.delete
    notify_image(current_user,"delete_image")
  end

  def like_image image
    @like = Like.where(user_id:current_user.id,likeable:image)
    if @like.exists?
      @like.take.delete
    else
      @like = image.likes.create(user_id:current_user.id)

    end
  end

  def notify_image current_user,content
    if current_user.follower_ids.present?
      @notify = NotificationService.new
      current_user.followers.each do |follower|
        @notify.create_notification follower,content,current_user
      end # each block
    end # if block
  end

  def history_image current_user,object
    @history = HistoryService.new
    @history.create_history(current_user,object)

  end

end
