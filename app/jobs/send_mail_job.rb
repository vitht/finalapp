class SendMailJob < ApplicationJob
  queue_as :mailer

  def perform(to_,user1,user2)
    FollowMailer.send_mail(to_,user1,user2).delivery_now
  end

end
