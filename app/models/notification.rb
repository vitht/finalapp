class Notification < ApplicationRecord
  belongs_to :user
  belongs_to :objecter, foreign_key: 'object_id', class_name: 'User'
end
