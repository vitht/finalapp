class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :albums, dependent: :destroy
  has_many :images
  has_many :likes
  has_many :follows, as: :followable
  has_many :followers, through: :follows, source: :user
  has_many :notifications
  has_many :histories, as: :itemable
  has_many :statuses

  # check permission for user

  def active_for_authentication?
    #remember to call the super
    #then put our own check to determine "active" state using
    #our own "is_active" column
    super and self.is_active?
  end
end
