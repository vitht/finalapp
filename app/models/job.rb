class Job
  include AASM
  aasm do
    state :sleeping, initial: true, before_enter: :do_something
    state :running
    state :finished
    state :idle, :initial => true
    state :cleaning

    after_all_transitions :log_status_change

    event :run do
      transitions from: :sleeping, to: :running
      # transitions from: :sleeping, to: :running, after: LongRunTime
      transitions :from => :running, :to => :finished, :after => LogRunTime
    end

    event :clean do
      transitions from: :idle, to: :cleaning, guard: :cleaning_needed?
    end


  end

  def cleaning_needed?
    false
  end

  def log_status_change
    puts "changing from #{aasm.from_state} to #{aasm.to_state} (event: #{aasm.current_event})"
  end

  def do_something
      puts "Trigger #{aasm.current_event}"
    end
end
