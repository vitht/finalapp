class Album < ApplicationRecord
  belongs_to :user, optional: true
  has_many :images, dependent: :destroy
  has_many :likes , as: :likeable
  has_many :follows, as: :followable
  has_many :followers, through: :follows, source: :user
  has_many :histories, as: :itemable



end
