class Status < ApplicationRecord
  belongs_to :user
  has_many :likes, as: :likeable
  has_many :histories, as: :itemable
end
