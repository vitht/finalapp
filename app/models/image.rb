class Image < ApplicationRecord
  scope :publiced, -> { where(is_private: false) }
  has_attached_file :image,styles: { thumb: "200x200#",large:"300x300#"}
  validates_attachment :image, presence: true, content_type: { content_type: /\Aimage\/.*\z/ }
  belongs_to :album, optional: true
  has_many :likes, as: :likeable
  belongs_to :user, optional: true
  has_many :histories, as: :itemable


end
