class Follow < ApplicationRecord
  belongs_to :user, optional:true
  belongs_to :followable, polymorphic: true
  after_create :send_mail


  def send_mail
    if(followable_type == 'User')
      byebug
    SendMailJob.perform_later followable,user.first_name,followable.first_name
  end

  end

end
