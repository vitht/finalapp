$(function(){
  $("#new_user").validate({
    onfocusout: function(element) { $(element).valid(); },
    rules:{
      "user[email]": {
        required: true
      },
      "user[password]":{
        required: true,
        minlength: 5
      }
    }
  })
})
