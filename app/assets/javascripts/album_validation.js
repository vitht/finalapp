$(function(){
  $("#new_album").validate({
    onfocusout: function(element) { $(element).valid(); },
    rules: {
      "album[title]": {
        required: true
      },
      "album[image][]":{
        required:true
      }

    }
  });
})

