$(function(){
  $("#new_image").validate({
    onfocusout: function(element) { $(element).valid(); },
    rules: {
      "image[title]": {
        required: true
      },
      "image[image]":{
        required:true
      }

    }
  });
  $("#edit_image").validate({
    onfocusout: function(element) { $(element).valid(); },
    rules: {
      "image[title]": {
        required: true
      }

    }
  });
  $("#add_image").validate({
    onfocusout: function(element) { $(element).valid(); },
    rules: {
      image : {
        required: true
      }

    }
  });

})
