$(function(){
  $("#sign_up").validate({
    onfocusout: function(element) { $(element).valid(); },
    rules:{
      "user[first_name]": {
        required: true
      },
      "user[last_name]": {
        required: true
      },
      "user[email]": {
        required: true
      },
      "user[password]":{
        required: true,
        minlength: 6
      },
      "user[password_confirmation]":{
        required: true,

      }
    }
  })
})

