class ImagesController < ApplicationController
  layout 'user'
  before_action :init_service
  before_action :authorize_image!, only: [:edit,:update,:destroy]
  before_action :get_image, only: [:show,:update,:destroy,:like]

  def index
     @images = Image.page(params[:page]).per(10)
  end

  def my_images
    @images = @image_service.get_images params
  end

  def public_images
    @images = @image_service.get_images params
  end

  def show
    @user = @image.user
    @check_follow = @follow_service.check_follow? @user
  end

  def new
    @album = Album.find(params[:album_id])
    @image = Image.new
  end

  def create
    @image = @image_service.add_image create_image_params
    redirect_to my_images_images_path
  end

  def edit
    if params[:album_id].present?
      @album = Album.find(params[:album_id])
      @image = Image.find(params[:id])
    else
      @image = Image.find(params[:id])
    end
  end

  def update
    @image_service.update_image @image,image_params
    if @image.album.present?
      redirect_to album_path
    else
       redirect_to my_images_images_path
    end
  end

  def destroy
    @image_service.delete_image @image
    redirect_to my_images_images_path
  end

  def like

    @image_service.like_image @image
    if params[:show] == "public_images"
      redirect_to public_images_images_path

    elsif params[:show] == "timeline_image"
      redirect_to timeline_user_path(current_user)

    else
      redirect_to my_images_images_path
    end

  end

  private

  def image_params
    params.require(:image).permit(:title, :description, :is_private,:image,:album_id)
  end

  def create_image_params
    params.permit(:image,:user_id)
  end

  def init_service
    @image_service ||= ImageService.new(current_user)
    @follow_service ||= FollowService.new(current_user)
  end

  def get_image
     @image = Image.find(params[:id])
  end

  def authorize_image!
    @image = current_user.images.find_by_id(params[:id])
    redirect_to(root_path, alert: I18n.t("image.permission") ) unless @image
  end

end
