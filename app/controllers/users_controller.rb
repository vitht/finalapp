class UsersController < ApplicationController
  layout 'user'
  before_action :init_service
  def show

  end

  def timeline
    @status = Status.new
    @histories = @history_service.get_histories params
  end

  def follow_user
    @object = User.find(params[:object_id])
    @follow_service.follow(@object)
    if @follow_service.check_follow? @object
      @notify_service.create_notification(current_user,"follow_user",
    @object)
    end
    redirect_to root_path
  end

  def notification
    @page,@per_page,@notifications = @notify_service.get_notification_list params
  end

  private
  def init_service
    @follow_service ||= FollowService.new(current_user)
    @notify_service ||= NotificationService.new(current_user)
    @history_service ||= HistoryService.new(current_user)
  end
end
