class StatusesController < ApplicationController
  layout 'User'
  before_action :init_service
  before_action :get_status, only: :like

  def create
    @status_service.create_status(status_params)
    redirect_to timeline_user_path(current_user)
  end

  def like
    @status_service.like_status(@status)
    redirect_to timeline_user_path(current_user)
  end

  private
  def authorize_status!
    @status = current_user.statuses.find_by_id(params[:id])
    redirect_to(root_path, alert: I18n.t("status.permission")) unless @status
  end

  def init_service
    @status_service ||= StatusService.new(current_user)
  end

  def status_params
    params.require(:status).permit(:content, :is_private)
  end

  def get_status
    @status = Status.find params[:id]
  end
end
