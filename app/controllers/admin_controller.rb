class AdminController < ApplicationController
  layout 'admin'
  before_action :check_admin!

  protected
  def check_admin!
    unless current_user.is_admin
      redirect_to root_path, alert: I18n.t("admin.permission")
    end
  end
end
