class AlbumsController < ApplicationController
  layout 'user'
  before_action :authorize_album!, only: [:edit, :update, :destroy]
  before_action :get_album, only: [:like,:follow_album]
  before_action :init_service

  def my_albums
    @albums = @album_service.get_albums params
  end

  def public_albums
    @albums = @album_service.get_albums params
  end

  def new
    @album = Album.new
  end

  def show
    if Album.exists?(id: params[:id])
      @album = Album.find(params[:id])
    else
      @image = Image.find params[:id]
      @album = @image.album
    end
    @images = @album.images.page(params[:page]).per(12).order(id: :desc)
    @count = @album.images.page(params[:page]).per(12)
  end

  def create
    @album = @album_service.create_album params,album_params
    redirect_to my_albums_albums_path(@album)
  end

  def edit

  end

  def update
    @album_service.update_album(@album,album_params)
    redirect_to  my_albums_albums_path
  end

  def like
    @album_service.like_album @album
    if params[:show] == "public_albums"
      redirect_to public_albums_albums_path
    elsif params[:show] == "timeline_album"
      redirect_to timeline_user_path(current_user)
    else
      redirect_to my_albums_albums_path
    end
  end

  def follow_album
    @follow_service.follow @album
    if @follow_service.check_follow? @object
      @notify_service.create_notification(current_user,"follow_album",
    @object)
    end
    redirect_to public_albums_albums_path
  end

  def destroy
    @album_service.delete_album @album
    redirect_to my_albums_albums_path
  end

  private
  def authorize_album!
    @album = current_user.albums.find_by_id(params[:id])
    redirect_to(root_path, alert: I18n.t("album.permission")) unless @album
  end

  def init_service
    @album_service ||= AlbumService.new(current_user)
    @follow_service ||= FollowService.new(current_user)
  end

  def album_params
    params.require(:album).permit(:title, :description, :is_private)
  end

  def get_album
    @album = Album.find params[:id]
  end
end
