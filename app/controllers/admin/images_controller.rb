class Admin::ImagesController < AdminController
  before_action :get_image , only: [:edit,:update,:destroy]
  before_action :init_service
  def index
    @page, @per_page, @images = @image_service.get_images_list params
  end

  def edit

  end

  def update
    @image.update(image_params)
    redirect_to admin_images_path
  end

  def destroy
    @image.destroy
    redirect_to admin_images_path
  end

  private
  def get_image
     @image = Image.find(params[:id])
  end

  def image_params
    params.require(:image).permit(:title, :description, :is_private,:image,:album_id)
  end
  def init_service
    @image_service ||= ImageService.new(current_user)
  end

end
