class Admin::UsersController < AdminController
  before_action :get_user ,only: [ :disable_user, :destroy]
  before_action :init_service

  def index
    @page, @per_page, @users = @user_service.get_users_list params
  end

  def new
    @user = User.new
  end

    # POST/users
    #POST/users.json

  def disable_user
    @user_service.disable_user @user
    redirect_to admin_users_path
  end

  def destroy
    @user.destroy
    redirect_to admin_users_path
  end

  private

  def get_user
    @user = User.find(params[:id])
  end

  def init_service
    @user_service ||= UserService.new
  end

  def user_params
    params.require(:user).permit(:avatar)
  end

end
