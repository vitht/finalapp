class FollowMailer < ApplicationMailer

  def send_mail(to_,user1,user2)
    subject = email_content_from(:subject,
      to: to_,
      user1: user1,
      user2: user2
      )
    content = email_content_from(:content,
      to: to_,
      user1: user1,
      user2: user2
      )
    # email_to: case to_.to_sym
    #           when :follow_user
    #             user2.email

    mail(to: to_, subject: subject)

  end

  def email_content_from(title,opts)
    options = opts
    I18n.t("follow.notify_changed_status.#{title}.follow_user", options)
  end
end
