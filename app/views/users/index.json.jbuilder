json.users @user do |user|
  json.name user.name
  json.email user.email
  json.password user.password
  json.phone user.phone
  json.dob user.dob
  json.status user.status
end
